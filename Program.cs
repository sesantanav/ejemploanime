﻿using System;
using static Anime.Anime;

namespace Anime
{
    class Program
    {
        static void Main(string[] args)
        {
            Anime anime1 = new Anime();

            string descripcion1 = "Anime basado en la historia de Gokú y sus amigos";

            anime1.settitulo("Dragon Ball");
            anime1.setDescripcion(descripcion1);

            anime1.getInformacion();
            Console.WriteLine(anime1.getDirector());

            Anime anime2 = new Anime();
            anime2.settitulo("Tokyo Revengers");
            anime2.setDescripcion("Anime que se trata de no se qué...");
            anime2.getInformacion();
        }
    }
}
