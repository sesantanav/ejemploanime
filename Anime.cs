﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anime
{
    class Anime
    {
        // Atributos
        private string titulo;
        private string genero;
        private string anno;
        private string descripcion;
        private string director;
        private string capitulos;

        // constructor
        public Anime() { }

        public Anime(string _titulo, string _genero, string _anno, string _descripcion, 
                    string _director, string _capitulos)
        {
            this.titulo = _titulo;
            this.genero = _genero;
            this.anno = _anno;
            this.descripcion = _descripcion;
            this.director = _director;
            this.capitulos = _capitulos;
        }
        // metodos

        public void settitulo(string _titulo)
        {
            this.titulo = _titulo;
        }

        public void setDescripcion(string _descripcion)
        {
            this.descripcion = _descripcion;
        }

        /// <summary>
        /// Metodo para obtener el titulo del anime
        /// </summary>
        /// <returns>Titulo del anime</returns>
        public string getTitulo()
        {
            return this.titulo;
        }

        public string getDirector()
        {
            return "Director : " + this.director;
        }

        /// <summary>
        /// Metodo para obtener información del titulo y descripcion del anime
        /// </summary>
        public void getInformacion()
        {
            Console.WriteLine("Titulo: " + this.titulo);
            Console.WriteLine("Descripcion: " + this.descripcion);
        }
    }
}
